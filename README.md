This dotCMS static configuration plugin performs the configuration of the dotCMS logs and the Apache Log4j library.

## Installation
### From Release
* Download the most recent release archive from [this plugin's repository's tags page](https://gitlab.msu.edu/canr/edu.msu.anr.config.logging/tags).
* Extract the release archive to the 'plugins' directory of your dotCMS instance. This should create the directory 'plugins\edu.msu.anr.config.logging'.

## Configuration
In order to configure the plugin for your specific instance, you will need to create and edit a 'plugin.properties' file in the plugin's 'conf' directory.

* Copy the file 'plugin.properties.dist' in the plugin's top level directory to 'plugin.properties'.
```
> cd plugins\edu.msu.anr.config.logging
> cp conf\plugin.properties.dist conf\plugin.properties
```
* Edit the file 'plugin.properties' to replace the dummy credentials with the credentials for your database.

You may also edit the log4j configuration file in 'ROOT/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/log4j/log4j2.xml'. See [the Apache Log4j 2 documentation](https://logging.apache.org/log4j/2.x/manual/configuration.html) for more info.

## Deploying
In order to deploy the configuration plugin to your dotCMS instance, you must run the dotCMS deploy plugins script. Note that this will deploy all of your static plugins.

```
> cd E:\dotcms\dotcms_x.y.z\
> .\bin\deploy-plugins.bat
```
